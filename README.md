<div align="center">
    <h1>
        <!--<img src="https://gitlab.com/raggesilver/color-picker/raw/master/data/icons/hicolor/scalable/apps/com.raggesilver.Proton.svg" />--> Color Picker
    </h1>
    <h4>Just a color picker that works on Wayland</h4>
    <p>
        <a href="https://gitlab.com/raggesilver/color-picker/pipelines">
            <img src="https://gitlab.com/raggesilver/color-picker/badges/master/pipeline.svg" alt="Build Status" />
        </a>
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="Support on Patreon" />
        </a>
    </p>
    <p>
        <a href="#install">Install</a> •
        <a href="#features">Features</a> •
        <a href="COPYING">License</a>
    </p>
</div>

Tired of not having one single working color picker on Wayland, here it is.

<div align="center">
    <img src="https://imgur.com/SzCjU8I.png">
</div>

## Features
- Works on Wayland
- History (10 most recent)
- rgb/hex

## Install

**Download**

[Flatpak](https://gitlab.com/raggesilver/color-picker/-/jobs/artifacts/master/file/color-picker.flatpak?job=deploy) • [Zip](https://gitlab.com/raggesilver/color-picker/-/jobs/artifacts/master/download?job=deploy)

*Note: these two links might not work if the latest pipeline failed/is still running*

## Compile

**Flatpak from source**

```bash
# Clone the repo
git clone https://gitlab.com/raggesilver/color-picker
# cd into the repo
cd color-picker
# Assuming you have both flatpak and flatpak-builder installed
sh test.sh
```

**Regular from source (unsupported)**

```bash
# Clone the repo
git clone --recursive https://gitlab.com/raggesilver/color-picker
# cd into the repo
cd color-picker
meson _build
ninja -C _build
# sudo
ninja -C _build install
```

*Note: Color Picker can be ran from GNOME Builder*
