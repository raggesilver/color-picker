/* Settings.vala
 *
 * Copyright 2020 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class ColorPicker.Settings : Marble.Settings
{
  private static Settings? instance = null;

  // public string[] recent_colors { get; set; }
  public string[] colors { get; set; }

  private Settings()
  {
    base("com.raggesilver.ColorPicker");
  }

  public static Settings get_default()
  {
    if (instance == null)
      instance = new Settings();
    return (instance);
  }

  public void push_color(string color)
  {
    string [] _col = {};

    _col += color;
    for (size_t i = 0; i < this.colors.length; i++)
    {
      if (_col.length >= 10) break;
      if (this.colors[i] in _col) continue;
      _col += this.colors[i];
    }
    this.colors = _col;
  }
}
