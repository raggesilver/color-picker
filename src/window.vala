/* window.vala
 *
 * Copyright 2020 Paulo Queiroz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
** Picker /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
**
** This is a class based on Gtk.ColorChooserWidget that makes an iternal child
** called picker_button publically available (needed by ColorPicker).
*/

internal class Picker : Gtk.ColorChooserWidget
{
  public Gtk.Button? picker_button { get; private set; }

  public Picker()
  {
    // Gtk.ColorChooserWidget children layout:
    //
    // Gtk.ColorChooserWidget
    //  - Box
    //  - Box <box1>
    //    - Gtk.ColorEditor
    //      - Gtk.Overlay (overlay)
    //        - Gtk.Grid (grid)
    //          - ...
    //          - Gtk.Button (picker_button)

    var box1 = this.get_children().next.data as Gtk.Box;
    var editor = box1.get_children().data as Gtk.Container;
    var over = editor.get_children().data as Gtk.Overlay;
    var grid = over.get_children().data as Gtk.Grid;

    debug("BOX1: %p", box1);
    debug("EDITOR: %p", editor);
    debug("OVER: %p", over);
    debug("GRID: %p", grid);

    this.picker_button = null;

    grid.forall((w) => {
      if (w.get_name() == "picker_button")
        this.picker_button = w as Gtk.Button;
    });

    debug("PICKER_BUTTON: %p", this.picker_button);

    if (this.picker_button == null)
      warning("Could not hack picker button");
  }
}

/*
** ColorPickerHistoryButton /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
*/

internal class ColorPicker.HistoryButton : Gtk.Button
{
  public Gdk.RGBA color { get; construct; }

  public HistoryButton(Gdk.RGBA color)
  {
    Object(color: color);
    // this.label = color;

    string data = "box { background: %s; }".printf(color.to_string());

    var b = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
    Marble.set_theming_for_data(b, data, null,
                                Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    this.get_style_context().add_class("color-button");
    this.add(b);
    this.show_all();
  }
}

/*
** Helper function that turns Gdk.RGBA to HEX format.
*/

internal string rgba_to_hex(Gdk.RGBA c)
{
  var r = (int) (c.red * 255);
  var g = (int) (c.green * 255);
  var b = (int) (c.blue * 255);

  return ("#%02X%02X%02X".printf(r, g, b));
}

/*
** ColorPickerWindow /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
*/

[GtkTemplate (ui = "/com/raggesilver/ColorPicker/layouts/window.ui")]
public class ColorPicker.Window : Gtk.ApplicationWindow
{
  /*
  ** Class signals =============================================================
  */
  // Emitted once the current color really changed
  private signal void real_updated();

  /*
  ** Class fields ==============================================================
  */

  private Gtk.AboutDialog? _about_window = null;
  private Settings settings = Settings.get_default();
  private Picker picker;
  private uint color_to = 0;

  // Used to indicate a recent update was internal, not from the user
  private bool internal_opr { get; private set; default = false; }

  /*
  ** Template children =========================================================
  */

  [GtkChild] Gtk.Box history_box;
  [GtkChild] Gtk.Box color_box; // The box inside the giant round button
  [GtkChild] Gtk.Label color_label;
  [GtkChild] Gtk.ComboBox color_mode_combo;
  [GtkChild] Gtk.Entry color_entry;

  /*
  ** Class methods =============================================================
  */

  public Window(Gtk.Application app)
  {
    Object(application: app);

    var css_provider = new Gtk.CssProvider();
    css_provider.load_from_resource(
      "/com/raggesilver/ColorPicker/resources/style.css");

    Gtk.StyleContext.add_provider_for_screen(
      Gdk.Screen.get_default(),
      css_provider,
      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    // TODO: Use GSettings for this
    Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;

    var sa = new SimpleAction("about", null);
    sa.activate.connect(this.on_about);
    this.add_action(sa);

    this.internal_opr = true;
    this.build_picker();

    this.color_mode_combo.notify["active-id"].connect(this.set_entry_text);
    this.real_updated.connect(this.set_entry_text);
    this.real_updated.connect(this.set_current_color);
    this.real_updated.connect(this.set_title_text);
    this.real_updated.connect(this.build_history);
    this.picker.notify["rgba"].connect(this.on_color_notify);

    if (this.settings.colors.length > 0)
    {
      Gdk.RGBA color = { 0 };

      if (color.parse(this.settings.colors[0]))
      {
        this.picker.rgba = color;
        this.real_updated();
      }
    }

    this.build_history();
    this.internal_opr = false;
  }

  private void build_picker()
  {
    this.picker = new Picker();
    this.picker.show_editor = true;
    this.picker.show();
    this.picker.use_alpha = false;
  }

  private void on_color_notify()
  {
    if (this.internal_opr)
    {
      return;
    }
    if (this.color_to != 0)
    {
      GLib.Source.remove(this.color_to);
    }
    this.color_to = GLib.Timeout.add(25, this.on_real_color_notify);
  }

  private bool on_real_color_notify()
  {
    this.color_to = 0;
    this.real_updated();
    return (false);
  }

  /*
  ** Functions that update the UI ==============================================
  */

  private void set_current_color()
  {
    var color = this.picker.rgba;
    string data = "box { background: %s; }".printf(color.to_string());

    Marble.set_theming_for_data(this.color_box, data, null,
                                Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
  }

  private void set_entry_text()
  {
    string color;

    if (this.color_mode_combo.active_id == "hex")
      color = rgba_to_hex(this.picker.rgba);
    else
      color = this.picker.rgba.to_string();

    this.color_entry.set_text(color);
  }

  private void set_title_text()
  {
    // The HEX color label bellow the huge round color button
    this.color_label.label = rgba_to_hex(this.picker.rgba);
  }

  private void build_history()
  {
    string[] recent = {};

    if (!this.internal_opr)
    {
      recent += rgba_to_hex(this.picker.rgba);
    }
    if (this.history_box.get_children().length() > 0)
    {
      this.history_box.foreach((w) => { this.history_box.remove(w); });
    }
    for (size_t i = 0; i < this.settings.colors.length; i++)
    {
      if (i >= 10) break;
      if (this.settings.colors[i] in recent) continue;
      recent += this.settings.colors[i];
    }
    foreach (string c in recent)
    {
      Gdk.RGBA col = { 0 };
      col.parse(c);
      var hb = new HistoryButton(col);

      hb.clicked.connect(() => {
        this.history_box.remove(hb);
        this.picker.rgba = col;
      });

      this.history_box.pack_start(hb, false, true, 0);
    }
    this.settings.colors = recent;
  }

  /*
  ** Template callbacks ========================================================
  */

  [GtkCallback]
  private void on_entry_action()
  {
    var cb = Gtk.Clipboard.get_default(Gdk.Display.get_default());
    cb.set_text(this.color_entry.get_text(), -1);
  }

  [GtkCallback]
  private void on_pick_cb()
  {
    this.picker.picker_button.clicked();
  }

  /*
  ** About Window action =======================================================
  */

  private void on_about()
  {
    var builder = new Gtk.Builder.from_resource(
      "/com/raggesilver/ColorPicker/layouts/about.ui");

    if (this._about_window != null)
    {
      this._about_window.show();
      return;
    }

    this._about_window = builder.get_object("window") as Gtk.AboutDialog;
    this._about_window.set_version(VERSION);
    this._about_window.set_transient_for(this);
    this._about_window.set_attached_to(this);
    this._about_window.show();

    this._about_window.destroy.connect(() => {
      this._about_window = null;
    });
  }
}
